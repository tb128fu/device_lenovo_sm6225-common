# All unpinned blobs below are extracted from TB128FU_CN_OPEN_USER_Q00012.0_S_ZUI_14.0.675_ST_221216

# ADSP
vendor/bin/adsprpcd
vendor/bin/dspservice
vendor/etc/init/vendor.qti.adsprpc-service.rc
vendor/etc/init/vendor.qti.hardware.dsp@1.0-service.rc
vendor/etc/seccomp_policy/vendor.qti.hardware.dsp.policy
vendor/lib/libadsp_default_listener.so
vendor/lib/libadsprpc.so
vendor/lib/vendor.qti.hardware.dsp@1.0.so
vendor/lib64/libadsp_default_listener.so
vendor/lib64/libadsprpc.so
vendor/lib64/vendor.qti.hardware.dsp@1.0.so

# Alarm
-product/app/PowerOffAlarm/PowerOffAlarm.apk
system_ext/framework/vendor.qti.hardware.alarm-V1.0-java.jar
vendor/bin/hw/vendor.qti.hardware.alarm@1.0-service
vendor/bin/power_off_alarm
vendor/etc/init/vendor.qti.hardware.alarm@1.0-service.rc
vendor/lib64/hw/vendor.qti.hardware.alarm@1.0-impl.so
vendor/lib64/vendor.qti.hardware.alarm@1.0.so

# ANT+
vendor/lib64/hw/com.dsi.ant@1.0-impl.so

# Bluetooth
vendor/bin/hw/android.hardware.bluetooth@1.0-service-qti
vendor/etc/init/android.hardware.bluetooth@1.0-service-qti.rc
vendor/lib64/hw/android.hardware.bluetooth@1.0-impl-qti.so
vendor/lib64/libbt-hidlclient.so
vendor/lib64/libbtnv.so

# Bluetooth (A2DP)
vendor/lib/hw/audio.bluetooth_qti.default.so
vendor/lib/hw/vendor.qti.hardware.bluetooth_audio@2.0-impl.so
vendor/lib/hw/vendor.qti.hardware.bluetooth_audio@2.1-impl.so
vendor/lib/hw/vendor.qti.hardware.bluetooth_sar@1.1-impl.so
vendor/lib/hw/vendor.qti.hardware.btconfigstore@1.0-impl.so
vendor/lib/hw/vendor.qti.hardware.btconfigstore@2.0-impl.so
vendor/lib/btaudio_offload_if.so
vendor/lib/libbluetooth_audio_session_qti.so
vendor/lib/libbluetooth_audio_session_qti_2_1.so
vendor/lib/libsoc_helper.so
vendor/lib/libsoc_helper_jni.so
vendor/lib/vendor.qti.hardware.bluetooth_sar@1.0.so
vendor/lib/vendor.qti.hardware.bluetooth_sar@1.1.so
vendor/lib64/hw/audio.bluetooth_qti.default.so
vendor/lib64/hw/vendor.qti.hardware.bluetooth_audio@2.0-impl.so
vendor/lib64/hw/vendor.qti.hardware.bluetooth_audio@2.1-impl.so
vendor/lib64/hw/vendor.qti.hardware.bluetooth_sar@1.1-impl.so
vendor/lib64/hw/vendor.qti.hardware.btconfigstore@1.0-impl.so
vendor/lib64/hw/vendor.qti.hardware.btconfigstore@2.0-impl.so
vendor/lib64/btaudio_offload_if.so
vendor/lib64/libbluetooth_audio_session_qti.so
vendor/lib64/libbluetooth_audio_session_qti_2_1.so
vendor/lib64/libsoc_helper.so
vendor/lib64/libsoc_helper_jni.so
vendor/lib64/vendor.qti.hardware.bluetooth_sar@1.0.so
vendor/lib64/vendor.qti.hardware.bluetooth_sar@1.1.so

# CDSP
vendor/bin/cdsprpcd
vendor/etc/init/vendor.qti.cdsprpc-service.rc
vendor/lib/libcdsp_default_listener.so
vendor/lib/libcdsprpc.so
vendor/lib/libfastcrc.so
vendor/lib/libmdsprpc.so
vendor/lib/libsdsprpc.so
vendor/lib/libsysmon_cdsp_skel.so
vendor/lib64/libcdfw.so
vendor/lib64/libcdfw_remote_api.so
vendor/lib64/libcdsp_default_listener.so
vendor/lib64/libcdsprpc.so
vendor/lib64/libmdsprpc.so
vendor/lib64/libsdsprpc.so
vendor/lib64/libsysmon_cdsp_skel.so

# Charger
vendor/bin/hvdcp_opti
vendor/bin/init.qti.chg_policy.sh
vendor/etc/init/vendor.qti.hardware.charger_monitor@1.0-service.rc
vendor/etc/charger_fstab.qti

# CNE
-vendor/app/CneApp/CneApp.apk
vendor/bin/cnd
vendor/etc/cne/mwqem.conf
vendor/etc/cne/profileMwqem.xml
vendor/etc/cne/wqeclient/ATT/ATT_profile1.xml
vendor/etc/cne/wqeclient/ATT/ATT_profile2.xml
vendor/etc/cne/wqeclient/ATT/ATT_profile3.xml
vendor/etc/cne/wqeclient/ATT/ATT_profile4.xml
vendor/etc/cne/wqeclient/ATT/ATT_profile5.xml
vendor/etc/cne/wqeclient/ATT/ATT_profile6.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile15.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile16.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile17.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile18.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile19.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile1.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile20.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile21.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile2.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile32.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile3.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile4.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile5.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile6.xml
vendor/etc/cne/wqeclient/ROW/ROW_profile7.xml
vendor/etc/cne/wqeclient/VZW/VZW_profile1.xml
vendor/etc/cne/wqeclient/VZW/VZW_profile2.xml
vendor/etc/cne/wqeclient/VZW/VZW_profile3.xml
vendor/etc/cne/wqeclient/VZW/VZW_profile4.xml
vendor/etc/cne/wqeclient/VZW/VZW_profile5.xml
vendor/etc/cne/wqeclient/VZW/VZW_profile6.xml
vendor/etc/init/cnd.rc
vendor/etc/default-permissions/com.qualcomm.qti.cne.xml
vendor/lib64/libcne.so
vendor/lib64/libcneapiclient.so
vendor/lib64/libcneoplookup.so
vendor/lib64/libwms.so
vendor/lib64/libwqe.so
vendor/lib64/libxml.so
vendor/lib64/vendor.qti.data.factory@2.0.so
vendor/lib64/vendor.qti.data.factory@2.1.so
vendor/lib64/vendor.qti.data.factory@2.2.so
vendor/lib64/vendor.qti.data.factory@2.3.so
vendor/lib64/vendor.qti.data.mwqem@1.0.so
vendor/lib64/vendor.qti.data.slm@1.0.so
vendor/lib64/vendor.qti.hardware.data.cne.internal.api@1.0.so
vendor/lib64/vendor.qti.hardware.data.cne.internal.constants@1.0.so
vendor/lib64/vendor.qti.hardware.data.cne.internal.server@1.0.so
vendor/lib64/vendor.qti.hardware.data.dynamicdds@1.0.so
vendor/lib64/vendor.qti.hardware.data.latency@1.0.so
vendor/lib64/vendor.qti.hardware.data.lce@1.0.so
vendor/lib64/vendor.qti.hardware.data.qmi@1.0.so
vendor/lib64/vendor.qti.hardware.mwqemadapter@1.0.so
vendor/lib64/vendor.qti.hardware.slmadapter@1.0.so
vendor/lib64/vendor.qti.latency@2.0.so
vendor/lib64/vendor.qti.latency@2.1.so

# Configstore
vendor/bin/hw/vendor.qti.hardware.capabilityconfigstore@1.0-service
vendor/etc/init/vendor.qti.hardware.capabilityconfigstore@1.0-service.rc
vendor/lib64/hw/vendor.qti.hardware.capabilityconfigstore@1.0-impl.so
vendor/lib/hw/vendor.qti.hardware.capabilityconfigstore@1.0-impl.so

# Display Calibration
vendor/bin/qdcmss
vendor/etc/init/qdcmss.rc
vendor/etc/display/advanced_sf_offsets.xml

# Display Calib Data
vendor/etc/qdcm_calib_data_hx83102p_boe_2k_video_mode_dsi_panel.xml
vendor/etc/qdcm_calib_data_nt36523w_tm_2k_video_mode_dsi_panel.xml

# Display HDR
vendor/lib64/libhdr_tm.so

# Display SDM
vendor/lib64/libdisplayqos.so
vendor/lib64/libdisplayskuutils.so
vendor/lib64/libdpp_manager.so
vendor/lib64/libdpps.so
vendor/lib64/libqseed3.so
vendor/lib64/librcmask.so
vendor/lib64/libsdm-color.so
vendor/lib64/libsdm-colormgr-algo.so
vendor/lib64/libsdm-diag.so
vendor/lib64/libsdm-disp-vndapis.so
vendor/lib64/libsdmextension.so
vendor/lib64/libtinyxml2_1.so
vendor/lib64/vendor.qti.hardware.qdutils_disp@1.0.so

# Display postprocessing
vendor/bin/hw/vendor.display.color@1.0-service
vendor/bin/ppd
vendor/etc/init/vendor.display.color@1.0-service.rc
vendor/lib64/libdisp-aba.so
vendor/lib64/vendor.display.color@1.0.so
vendor/lib64/vendor.display.color@1.1.so
vendor/lib64/vendor.display.color@1.2.so
vendor/lib64/vendor.display.color@1.3.so
vendor/lib64/vendor.display.color@1.4.so
vendor/lib64/vendor.display.color@1.5.so
vendor/lib64/vendor.display.postproc@1.0.so

# DPM
framework/tcmclient.jar
system_ext/bin/dpmd
system_ext/etc/dpm/dpm.conf
system_ext/etc/init/dpmd.rc
system_ext/etc/permissions/com.qti.dpmframework.xml
system_ext/etc/permissions/dpmapi.xml
system_ext/framework/com.qti.dpmframework.jar
system_ext/framework/dpmapi.jar
system_ext/lib/com.qualcomm.qti.dpm.api@1.0.so
system_ext/lib/libdpmctmgr.so
system_ext/lib/libdpmfdmgr.so
system_ext/lib/libdpmframework.so
system_ext/lib/libdpmtcm.so
system_ext/lib64/com.qualcomm.qti.dpm.api@1.0.so
system_ext/lib64/libdpmctmgr.so
system_ext/lib64/libdpmfdmgr.so
system_ext/lib64/libdpmframework.so
system_ext/lib64/libdpmtcm.so
-system_ext/priv-app/dpmserviceapp/dpmserviceapp.apk
vendor/bin/dpmQmiMgr
vendor/etc/init/dpmQmiMgr.rc
vendor/lib64/com.qualcomm.qti.dpm.api@1.0.so
vendor/lib64/libdpmqmihal.so

# DRM
vendor/bin/hw/vendor.qti.hardware.qseecom@1.0-service
vendor/bin/qseecomd
vendor/etc/gpfspath_oem_config.xml
vendor/etc/init/qseecomd.rc
vendor/etc/init/vendor.qti.hardware.qseecom@1.0-service.rc
vendor/lib64/hw/vendor.qti.hardware.qseecom@1.0-impl.so
vendor/lib64/libbase64.so
vendor/lib64/libcpion.so
vendor/lib64/libdrmfs.so
vendor/lib64/libdrmtime.so
vendor/lib64/libGPreqcancel.so
vendor/lib64/libGPreqcancel_svc.so
vendor/lib64/libops.so
vendor/lib64/libqisl.so
vendor/lib64/libQSEEComAPI.so
vendor/lib64/librpmb.so
vendor/lib64/libsecureui.so
vendor/lib64/libsecureui_svcsock.so
vendor/lib64/libSecureUILib.so
vendor/lib64/libssd.so
vendor/lib64/libStDrvInt.so
vendor/lib64/libtzdrmgenprov.so
vendor/lib64/vendor.qti.hardware.qseecom@1.0.so

# Embedded Secure Element power manager
vendor/lib64/vendor.qti.esepowermanager@1.0.so
vendor/lib64/vendor.qti.esepowermanager@1.1.so

# FM
system_ext/lib64/fm_helium.so
system_ext/lib64/libfm-hci.so
system_ext/lib64/vendor.qti.hardware.fm@1.0.so
vendor/lib64/hw/vendor.qti.hardware.fm@1.0-impl.so
vendor/lib64/vendor.qti.hardware.fm@1.0.so

# Gatekeeper
vendor/bin/hw/android.hardware.gatekeeper@1.0-service-qti
vendor/etc/init/android.hardware.gatekeeper@1.0-service-qti.rc
vendor/lib64/hw/android.hardware.gatekeeper@1.0-impl-qti.so

# GNSS
vendor/bin/hw/android.hardware.gnss@2.1-service-qti
vendor/bin/loc_launcher
vendor/bin/lowi-server
vendor/bin/mlid
vendor/bin/slim_daemon
vendor/bin/xtra-daemon
vendor/bin/xtwifi-client
vendor/bin/xtwifi-inet-agent
vendor/etc/init/android.hardware.gnss@2.1-service-qti.rc
vendor/etc/seccomp_policy/gnss@2.0-base.policy
vendor/etc/seccomp_policy/gnss@2.0-xtra-daemon.policy
vendor/etc/seccomp_policy/gnss@2.0-xtwifi-client.policy
vendor/etc/seccomp_policy/gnss@2.0-xtwifi-inet-agent.policy
-vendor/etc/vintf/manifest/android.hardware.gnss@2.1-service-qti.xml
-vendor/etc/vintf/manifest/vendor.qti.gnss@4.0-service.xml
vendor/etc/cacert_location.pem
vendor/etc/xtra_root_cert.pem
vendor/lib64/hw/android.hardware.gnss@2.1-impl-qti.so
vendor/lib64/hw/vendor.qti.gnss@4.0-impl.so
vendor/lib64/libaoa.so
vendor/lib64/libasn1cper.so
vendor/lib64/libbatching.so
vendor/lib64/libcacertclient.so
vendor/lib64/libdataitems.so
vendor/lib64/libgdtap.so
vendor/lib64/libgeofencing.so
vendor/lib64/libgnss.so
vendor/lib64/libgnsspps.so
vendor/lib64/libgps.utils.so
vendor/lib64/libizat_client_api.so
vendor/lib64/libizat_core.so
vendor/lib64/libjnihelper.so
vendor/lib64/liblbs_core.so
vendor/lib64/libloc_api_v02.so
vendor/lib64/libloc_api_wds.so
vendor/lib64/libloc_core.so
vendor/lib64/libloc_socket.so
vendor/lib64/liblocation_api.so
vendor/lib64/liblocationservice.so
vendor/lib64/liblocationservice_glue.so
vendor/lib64/liblowi_client.so
vendor/lib64/liblowi_wifihal.so
vendor/lib64/libminksocket.so
vendor/lib64/libqcc_file_agent.so
vendor/lib64/libqdma_file_agent.so
vendor/lib64/libslimclient.so
vendor/lib64/libsynergy_loc_api.so
vendor/lib64/libxtadapter.so
vendor/lib64/libxtwifi_server_protocol.so
vendor/lib64/libxtwifi_server_protocol_uri_v3.so
vendor/lib64/vendor.qti.gnss@1.0.so
vendor/lib64/vendor.qti.gnss@1.1.so
vendor/lib64/vendor.qti.gnss@1.2.so
vendor/lib64/vendor.qti.gnss@2.0.so
vendor/lib64/vendor.qti.gnss@2.1.so
vendor/lib64/vendor.qti.gnss@3.0.so
vendor/lib64/vendor.qti.gnss@4.0-service.so
vendor/lib64/vendor.qti.gnss@4.0.so

# GPS
vendor/etc/apdr.conf
vendor/etc/flp.conf
vendor/etc/gps.conf
vendor/etc/izat.conf
vendor/etc/lowi.conf
vendor/etc/sap.conf
vendor/etc/xtwifi.conf

# Graphics Adreno
vendor/lib/egl/eglSubDriverAndroid.so
vendor/lib/egl/libGLESv1_CM_adreno.so
vendor/lib/egl/libq3dtools_adreno.so
vendor/lib/libllvm-qcom.so
vendor/lib/libOpenCL.so
vendor/lib64/egl/eglSubDriverAndroid.so
vendor/lib64/egl/libGLESv1_CM_adreno.so
vendor/lib64/egl/libq3dtools_adreno.so
vendor/lib64/libllvm-qcom.so
vendor/lib64/libOpenCL.so

# Graphics Adreno - from devon_g S2SNS32.34-72-14-1
vendor/lib/libadreno_app_profiles.so|25925fd0a03e0dc3b4f7f097a9ee0a1808667b3c
vendor/lib64/libadreno_app_profiles.so|a101de51b48321f15d24d67fe2928a22a65da911

# Graphics Adreno - from miui_SPESGlobal_V14.0.2.0.TGCMIXM_f4e2e7b209_13.0
vendor/lib/egl/libEGL_adreno.so|2b96d33f61340c391cc2a6c503bff6b12f2659ed
vendor/lib/egl/libGLESv2_adreno.so|108e6cd874dacee4ff02638d694d2d62cbbdf25f
vendor/lib/egl/libq3dtools_esx.so|25ca8fee48e36cdae2c9c434ab3f72959b0e8949
vendor/lib/libadreno_utils.so|97e4665f049157f718c6e59910e1d7d83769a9c3
vendor/lib/libC2D2.so|c1916f2adb917f3567b872670496994456472586
vendor/lib/libc2d30_bltlib.so|686415649550e0258adf85219ca0f7af3ecc28f9
vendor/lib/libCB.so|02dc1a1c96b1e2da416af00225a013fd43190d4a
vendor/lib/libgpudataproducer.so|9998b6752ee2c28c3f152233973acfa24af4be24
vendor/lib/libgsl.so|9f2d96e8caafc739f5665a7bbd65144206e9eba6
vendor/lib/libllvm-glnext.so|3fea92e9d8fa3147b48b8f104c8639ea32261c20
vendor/lib/libVkLayer_q3dtools.so|8eee9b45ebf9d2e4102513677e525eabf93b925e
vendor/lib64/egl/libEGL_adreno.so|59cb197303a0374b11e9db751052a4e2fbfae7c4
vendor/lib64/egl/libGLESv2_adreno.so|51c7d174b8123f6015cbdb66476147bbc9ca70b1
vendor/lib64/egl/libq3dtools_esx.so|3623b91a25d154ec643e1d7ba94cb8b7c4320478
vendor/lib64/libadreno_utils.so|1eb1fd83aa58a9206528c8ebf8648d2caa06600a
vendor/lib64/libC2D2.so|2f5bb4e82006bcc854aa468126bd3fffab339f67
vendor/lib64/libc2d30_bltlib.so|3cf4e37f2c36fe25eeb8da60474c2274e7bd7ebf
vendor/lib64/libCB.so|37db85c8db24c32998bcdc4a93abf7669ad8939e
vendor/lib64/libgpudataproducer.so|263efe28081d23e2644097c5dd40a39fe94756ba
vendor/lib64/libgsl.so|53283ed9680381e7b19a3f702f2527a7816b128b
vendor/lib64/libllvm-glnext.so|ed677107cd305708586d85083a0783ac703d580d
vendor/lib64/libVkLayer_q3dtools.so|a5f5a3c227f3206c2f87068e1d437ad76326efb0

# Graphics Vulkan - from miui_SPESGlobal_V14.0.2.0.TGCMIXM_f4e2e7b209_13.0
vendor/lib/hw/vulkan.adreno.so|bf9536e9b2b8174cedd28eb1b2f11095670d9cfe
vendor/lib64/hw/vulkan.adreno.so|9b2eaf02a455cc9da4080ca7f123f0102dea8a7a

# IMS
-system_ext/app/uceShimService/uceShimService.apk
system_ext/framework/vendor.qti.ims.factory-V1.0-java.jar
system_ext/framework/vendor.qti.ims.factory-V1.1-java.jar
system_ext/lib64/lib-imsvideocodec.so
system_ext/lib64/lib-imsvt.so
system_ext/lib64/lib-imsvtextutils.so
system_ext/lib64/lib-imsvtutils.so
system_ext/lib64/libdiag_system.so
system_ext/lib64/libimscamera_jni.so
system_ext/lib64/libimsmedia_jni.so
system_ext/lib64/vendor.qti.diaghal@1.0.so
system_ext/lib64/vendor.qti.ims.factory@1.0.so
system_ext/lib64/vendor.qti.ims.factory@1.1.so
system_ext/lib64/vendor.qti.ims.rcsconfig@1.0.so
system_ext/lib64/vendor.qti.ims.rcsconfig@1.1.so
system_ext/lib64/vendor.qti.ims.rcsconfig@2.0.so
system_ext/lib64/vendor.qti.ims.rcsconfig@2.1.so
system_ext/lib64/vendor.qti.imsrtpservice@3.0.so
-system_ext/priv-app/ims/ims.apk
vendor/bin/ims_rtp_daemon
vendor/bin/imsdatadaemon
vendor/bin/imsqmidaemon
vendor/bin/imsrcsd
vendor/etc/init/ims_rtp_daemon.rc
vendor/etc/init/imsdatadaemon.rc
vendor/etc/init/imsqmidaemon.rc
vendor/etc/init/imsrcsd.rc
vendor/etc/seccomp_policy/imsrtp.policy
vendor/lib64/com.qualcomm.qti.imscmservice@1.0.so
vendor/lib64/com.qualcomm.qti.imscmservice@2.0.so
vendor/lib64/com.qualcomm.qti.imscmservice@2.1.so
vendor/lib64/com.qualcomm.qti.imscmservice@2.2.so
vendor/lib64/com.qualcomm.qti.uceservice@2.0.so
vendor/lib64/com.qualcomm.qti.uceservice@2.1.so
vendor/lib64/com.qualcomm.qti.uceservice@2.2.so
vendor/lib64/com.qualcomm.qti.uceservice@2.3.so
vendor/lib64/lib-imscmservice.so
vendor/lib64/lib-imsdpl.so
vendor/lib64/lib-imsqimf.so
vendor/lib64/lib-imsrcs-v2.so
vendor/lib64/lib-imsrcsbaseimpl.so
vendor/lib64/lib-imsvtcore.so
vendor/lib64/lib-imsxml.so
vendor/lib64/lib-rcsconfig.so
vendor/lib64/lib-rtpcommon.so
vendor/lib64/lib-rtpcore.so
vendor/lib64/lib-rtpsl.so
vendor/lib64/lib-siputility.so
vendor/lib64/lib-uceservice.so
vendor/lib64/librcc.so
vendor/lib64/vendor.qti.hardware.radio.ims@1.0.so
vendor/lib64/vendor.qti.hardware.radio.ims@1.1.so
vendor/lib64/vendor.qti.hardware.radio.ims@1.2.so
vendor/lib64/vendor.qti.hardware.radio.ims@1.3.so
vendor/lib64/vendor.qti.hardware.radio.ims@1.4.so
vendor/lib64/vendor.qti.hardware.radio.ims@1.5.so
vendor/lib64/vendor.qti.hardware.radio.ims@1.6.so
vendor/lib64/vendor.qti.hardware.radio.ims@1.7.so
vendor/lib64/vendor.qti.ims.callcapability@1.0.so
vendor/lib64/vendor.qti.ims.callinfo@1.0.so
vendor/lib64/vendor.qti.ims.factory@1.0.so
vendor/lib64/vendor.qti.ims.factory@1.1.so
vendor/lib64/vendor.qti.ims.rcsconfig@1.0.so
vendor/lib64/vendor.qti.ims.rcsconfig@1.1.so
vendor/lib64/vendor.qti.ims.rcsconfig@2.0.so
vendor/lib64/vendor.qti.ims.rcsconfig@2.1.so
vendor/lib64/vendor.qti.imsrtpservice@3.0-service-Impl.so
vendor/lib64/vendor.qti.imsrtpservice@3.0.so

# IO prefetcher
vendor/bin/hw/vendor.qti.hardware.iop@2.0-service
vendor/etc/init/vendor.qti.hardware.iop@2.0-service.rc
vendor/lib64/libqti-iopd-client.so
vendor/lib64/libqti-iopd.so
vendor/lib64/vendor.qti.hardware.iop@1.0.so
vendor/lib64/vendor.qti.hardware.iop@2.0.so

# IRQ balance
vendor/bin/msm_irqbalance
vendor/etc/msm_irqbalance.conf

# IPA firmware
vendor/etc/init/ipa_fws.rc
vendor/firmware/ipa_fws.b00
vendor/firmware/ipa_fws.b01
vendor/firmware/ipa_fws.b02
vendor/firmware/ipa_fws.b03
vendor/firmware/ipa_fws.b04
vendor/firmware/ipa_fws.elf
vendor/firmware/ipa_fws.mdt
vendor/firmware/scuba_ipa_fws.b00
vendor/firmware/scuba_ipa_fws.b01
vendor/firmware/scuba_ipa_fws.b02
vendor/firmware/scuba_ipa_fws.b03
vendor/firmware/scuba_ipa_fws.b04
vendor/firmware/scuba_ipa_fws.elf
vendor/firmware/scuba_ipa_fws.mdt

# Keymaster
vendor/bin/hw/android.hardware.keymaster@4.1-service-qti
vendor/etc/init/android.hardware.keymaster@4.1-service-qti.rc
-vendor/etc/vintf/manifest/android.hardware.keymaster@4.1-service-default-qti.xml
vendor/lib64/libkeymasterdeviceutils.so
vendor/lib64/libkeymasterprovision.so
vendor/lib64/libkeymasterutils.so
vendor/lib64/libqtikeymaster4.so

# Listen
vendor/lib/hw/sound_trigger.primary.bengal.so
vendor/lib/libadpcmdec.so
vendor/lib/libcapiv2svacnn.so
vendor/lib/libcapiv2vop.so
vendor/lib/libgcs-calwrapper.so
vendor/lib/libgcs-ipc.so
vendor/lib/libgcs-osal.so
vendor/lib/libgcs.so
vendor/lib/liblistensoundmodel2.so
vendor/lib/libmulawdec.so
vendor/lib/libsmwrapper.so

# Media
system_ext/lib/libmmosal.so
system_ext/lib/libmmparser_lite.so
system_ext/lib64/libmmosal.so
system_ext/lib64/libmmparser_lite.so
vendor/etc/seccomp_policy/mediacodec.policy
vendor/lib/libmmosal.so
vendor/lib64/libmmosal.so

# Media OMX
vendor/lib/libAlacSwDec.so
vendor/lib/libApeSwDec.so
vendor/lib/libdsd2pcm.so
vendor/lib/libFlacSwDec.so
vendor/lib/libMpeg4SwEncoder.so
vendor/lib/libMpeghSwEnc.so
vendor/lib/libOmxAacDec.so
vendor/lib/libOmxAlacDec.so
vendor/lib/libOmxAlacDecSw.so
vendor/lib/libOmxAmrDec.so
vendor/lib/libOmxAmrwbplusDec.so
vendor/lib/libOmxApeDec.so
vendor/lib/libOmxApeDecSw.so
vendor/lib/libOmxDsdDec.so
vendor/lib/libOmxEvrcDec.so
vendor/lib/libOmxG711Dec.so
vendor/lib/libOmxMpeghDecSw.so
vendor/lib/libOmxMpeghEncSw.so
vendor/lib/libOmxQcelp13Dec.so
vendor/lib/libOmxSwVdec.so
vendor/lib/libOmxSwVencMpeg4.so
vendor/lib/libOmxVideoDSMode.so
vendor/lib/libOmxWmaDec.so
vendor/lib/libswvdec.so
vendor/lib64/libAlacSwDec.so
vendor/lib64/libApeSwDec.so
vendor/lib64/libFlacSwDec.so
vendor/lib64/libMpeg4SwEncoder.so
vendor/lib64/libMpeghSwEnc.so
vendor/lib64/libOmxAacDec.so
vendor/lib64/libOmxAlacDec.so
vendor/lib64/libOmxAlacDecSw.so
vendor/lib64/libOmxAmrDec.so
vendor/lib64/libOmxAmrwbplusDec.so
vendor/lib64/libOmxApeDec.so
vendor/lib64/libOmxApeDecSw.so
vendor/lib64/libOmxEvrcDec.so
vendor/lib64/libOmxG711Dec.so
vendor/lib64/libOmxMpeghEncSw.so
vendor/lib64/libOmxQcelp13Dec.so
vendor/lib64/libOmxSwVdec.so
vendor/lib64/libOmxSwVencMpeg4.so
vendor/lib64/libOmxWmaDec.so
vendor/lib64/libswvdec.so

# Neural networks
vendor/bin/hw/android.hardware.neuralnetworks@1.3-service-qti
vendor/etc/init/android.hardware.neuralnetworks@1.3-service-qti.rc
-vendor/etc/vintf/manifest/android.hardware.neuralnetworks@1.3-service-qti.xml
vendor/lib64/unnhal-acc-adreno.so
vendor/lib64/unnhal-acc-common.so
vendor/lib64/unnhal-acc-hvx.so

# Perf
vendor/bin/hw/vendor.qti.hardware.perf@2.2-service
vendor/etc/init/vendor.qti.hardware.perf@2.2-service.rc
vendor/etc/perf/commonresourceconfigs.xml
vendor/etc/perf/commonsysnodesconfigs.xml
vendor/etc/perf/perfboostsconfig.xml
vendor/etc/perf/perfconfigstore.xml
vendor/etc/perf/targetconfig.xml
vendor/etc/perf/targetresourceconfigs.xml
vendor/etc/lm/AdaptLaunchFeature.xml
vendor/etc/lm/AppClassifierFeature.xml
vendor/etc/lm/GameOptimizationFeature.xml
vendor/etc/lm/PreKillFeature.xml
vendor/etc/lm/prekill/prekill_2GB.data
vendor/etc/lm/prekill/prekill_4GB.data
vendor/etc/lm/prekill/prekill_6GB.data
vendor/etc/powerhint.xml
vendor/lib/libperfconfig.so
vendor/lib/libperfgluelayer.so
vendor/lib/libperfioctl.so
vendor/lib/libqti-perfd-client.so
vendor/lib/libqti-perfd.so
vendor/lib/libqti-util.so
vendor/lib/vendor.qti.memory.pasrmanager@1.0.so
vendor/lib/vendor.qti.memory.pasrmanager@1.1.so
vendor/lib64/libadaptlaunch.so
vendor/lib64/libappclassifier.so
vendor/lib64/libgameoptfeature.so
vendor/lib64/liblearningmodule.so
vendor/lib64/liblmutils-ns.so
vendor/lib64/libmemperfd.so
vendor/lib64/libmeters.so
vendor/lib64/libmeters-ns.so
vendor/lib64/libperfconfig.so
vendor/lib64/libperfgluelayer.so
vendor/lib64/libperfioctl.so
vendor/lib64/libprekill.so
vendor/lib64/libqti-perfd-client.so
vendor/lib64/libqti-perfd.so
vendor/lib64/libqti-util.so
vendor/lib64/vendor.qti.memory.pasrmanager@1.0.so
vendor/lib64/vendor.qti.memory.pasrmanager@1.1.so

# Perf - from LA.QSSI.12.0.r1-05600-qssi.0
etc/permissions/com.qualcomm.qti.Performance.xml|7976f2fd46b8d07814310cc957a70f829bdd6932
etc/permissions/com.qualcomm.qti.UxPerformance.xml|d92749d80c5d956db8251b08ea6388d3f107a32c

# Perf
lib64/libtflite.so
-system_ext/app/PerformanceMode/PerformanceMode.apk
-system_ext/app/workloadclassifier/workloadclassifier.apk
system_ext/bin/perfservice
system_ext/bin/qspmsvc
system_ext/etc/init/perfservice.rc
system_ext/etc/init/qspmsvc.rc
system_ext/etc/perf/wlc_model.tflite
system_ext/etc/seccomp_policy/perfservice.policy
system_ext/lib64/libbeluga.so
system_ext/lib64/libcomposerextn.qti.so
system_ext/lib64/liblayerext.qti.so
system_ext/lib64/libqspmsvc.so
system_ext/lib64/libqti-at.so
system_ext/lib64/libqti-perfd-client_system.so
system_ext/lib64/libqti-iopd-client_system.so
system_ext/lib64/libqti_performance.so
system_ext/lib64/libqti_workloadclassifiermodel.so
system_ext/lib64/libskewknob_system.so
system_ext/lib64/libsmomoconfig.qti.so
system_ext/lib64/vendor.qti.hardware.iop@2.0.so
system_ext/lib64/vendor.qti.hardware.limits@1.0.so
system_ext/lib64/vendor.qti.hardware.limits@1.1.so
system_ext/lib64/vendor.qti.qspmhal@1.0.so
-framework/QPerformance.jar
-framework/QXPerformance.jar
-framework/UxPerformance.jar

# Peripheral manager
vendor/bin/pm-proxy
vendor/bin/pm-service
vendor/lib64/libperipheral_client.so

# QCC
vendor/bin/hw/vendor.qti.hardware.qccvndhal@1.0-service
vendor/etc/init/vendor.qti.hardware.qccvndhal@1.0-service.rc
vendor/lib64/hw/vendor.qti.hardware.qccvndhal@1.0-impl.so
vendor/lib64/vendor.qti.hardware.qccsyshal@1.0.so
vendor/lib64/vendor.qti.hardware.qccvndhal@1.0-halimpl.so
vendor/lib64/vendor.qti.hardware.qccvndhal@1.0.so

# QMI
etc/permissions/privapp-permissions-qti.xml
etc/sysconfig/qti_whitelist.xml
vendor/bin/irsc_util
vendor/etc/sec_config
vendor/lib/libdiag.so
vendor/lib/libdsi_netctrl.so
vendor/lib/libdsutils.so
vendor/lib/libidl.so
vendor/lib/libqcci_legacy.so
vendor/lib/libqcmaputils.so
vendor/lib/libqdi.so
vendor/lib/libqdp.so
vendor/lib/libqmi.so
vendor/lib/libqmi_cci.so
vendor/lib/libqmi_client_helper.so
vendor/lib/libqmi_client_qmux.so
vendor/lib/libqmi_common_so.so
vendor/lib/libqmi_csi.so
vendor/lib/libqmi_encdec.so
vendor/lib/libqmi_legacy.so
vendor/lib/libqmiservices.so
vendor/lib64/libdiag.so
vendor/lib64/libdsi_netctrl.so
vendor/lib64/libdsutils.so
vendor/lib64/libidl.so
vendor/lib64/libqcci_legacy.so
vendor/lib64/libqcmaputils.so
vendor/lib64/libqdi.so
vendor/lib64/libqdp.so
vendor/lib64/libqmi.so
vendor/lib64/libqmi_cci.so
vendor/lib64/libqmi_client_helper.so
vendor/lib64/libqmi_client_qmux.so
vendor/lib64/libqmi_common_so.so
vendor/lib64/libqmi_csi.so
vendor/lib64/libqmi_encdec.so
vendor/lib64/libqmi_legacy.so
vendor/lib64/libqmiservices.so

# QSP
vendor/bin/vendor.qti.qspmhal@1.0-service
vendor/etc/init/vendor.qti.qspmhal@1.0-service.rc
vendor/etc/seccomp_policy/qspm.policy
vendor/lib/vendor.qti.qspmhal@1.0-impl.so
vendor/lib/vendor.qti.qspmhal@1.0.so
vendor/lib64/vendor.qti.qspmhal@1.0-impl.so
vendor/lib64/vendor.qti.qspmhal@1.0.so

# RIL
-system_ext/app/QtiTelephonyService/QtiTelephonyService.apk
system_ext/etc/permissions/qcrilhook.xml
system_ext/etc/permissions/qti_libpermissions.xml
system_ext/etc/permissions/qti_permissions.xml
system_ext/etc/permissions/telephony_system-ext_privapp-permissions-qti.xml
system_ext/framework/qcrilhook.jar
-system_ext/priv-app/qcrilmsgtunnel/qcrilmsgtunnel.apk
-vendor/app/IWlanService/IWlanService.apk
vendor/bin/adpl
vendor/bin/ks
vendor/bin/netmgrd
vendor/bin/pd-mapper
vendor/bin/port-bridge
vendor/bin/qrtr-cfg
vendor/bin/qrtr-lookup
vendor/bin/qrtr-ns
vendor/bin/qti
vendor/bin/rmt_storage
vendor/bin/ssgtzd
vendor/bin/tftp_server
vendor/etc/data/dsi_config.xml
vendor/etc/data/netmgr_config.xml
vendor/etc/ssg/ta_config.json
vendor/etc/ssg/tz_whitelist.json
vendor/etc/init/dataadpl.rc
vendor/etc/init/dataqti.rc
vendor/etc/init/init-qcril-data.rc
vendor/etc/init/netmgrd.rc
vendor/etc/init/port-bridge.rc
vendor/etc/init/ssgtzd.rc
vendor/etc/init/vendor.qti.rmt_storage.rc
vendor/etc/init/vendor.qti.tftp.rc
vendor/lib/libnetmgr.so
vendor/lib/libmdmdetect.so
vendor/lib64/libconfigdb.so
vendor/lib64/liblqe.so
vendor/lib64/libmdmdetect.so
vendor/lib64/libnetmgr.so
vendor/lib64/libnetmgr_common.so
vendor/lib64/libnetmgr_nr_fusion.so
vendor/lib64/libnetmgr_rmnet_ext.so
vendor/lib64/libnlnetmgr.so
vendor/lib64/libpdmapper.so
vendor/lib64/libpdnotifier.so
vendor/lib64/libqcbor.so
vendor/lib64/libqcrildatactl.so
vendor/lib64/libqrtr.so
vendor/lib64/libqsocket.so
vendor/lib64/libsystem_health_mon.so
vendor/lib64/vendor.qti.hardware.data.connection@1.0.so
vendor/lib64/vendor.qti.hardware.data.connection@1.1.so
vendor/lib64/vendor.qti.hardware.radio.am@1.0.so
vendor/lib64/vendor.qti.hardware.radio.atcmdfwd@1.0.so
vendor/lib64/vendor.qti.hardware.radio.internal.deviceinfo@1.0.so
vendor/lib64/vendor.qti.hardware.radio.lpa@1.0.so
vendor/lib64/vendor.qti.hardware.radio.lpa@1.1.so
vendor/lib64/vendor.qti.hardware.radio.lpa@1.2.so
vendor/lib64/vendor.qti.hardware.radio.qcrilhook@1.0.so
vendor/lib64/vendor.qti.hardware.radio.qtiradio@1.0.so
vendor/lib64/vendor.qti.hardware.radio.qtiradio@2.0.so
vendor/lib64/vendor.qti.hardware.radio.qtiradio@2.1.so
vendor/lib64/vendor.qti.hardware.radio.qtiradio@2.2.so
vendor/lib64/vendor.qti.hardware.radio.qtiradio@2.3.so
vendor/lib64/vendor.qti.hardware.radio.qtiradio@2.4.so
vendor/lib64/vendor.qti.hardware.radio.qtiradio@2.5.so
vendor/lib64/vendor.qti.hardware.radio.qtiradio@2.6.so
vendor/lib64/vendor.qti.hardware.radio.uim@1.0.so
vendor/lib64/vendor.qti.hardware.radio.uim@1.1.so
vendor/lib64/vendor.qti.hardware.radio.uim@1.2.so
vendor/lib64/vendor.qti.hardware.radio.uim_remote_client@1.0.so
vendor/lib64/vendor.qti.hardware.radio.uim_remote_client@1.1.so
vendor/lib64/vendor.qti.hardware.radio.uim_remote_client@1.2.so
vendor/lib64/vendor.qti.hardware.radio.uim_remote_server@1.0.so

# Sensors Calibrate
vendor/bin/hw/vendor.qti.hardware.sensorscalibrate@1.0-service
vendor/etc/init/vendor.qti.hardware.sensorscalibrate@1.0-service.rc
vendor/etc/permissions/vendor-qti-hardware-sensorscalibrate.xml
vendor/lib64/hw/vendor.qti.hardware.sensorscalibrate@1.0-impl.so
vendor/lib64/vendor.qti.hardware.sensorscalibrate@1.0.so

# Service Tracker QTI
vendor/bin/hw/vendor.qti.hardware.servicetracker@1.2-service
vendor/etc/init/vendor.qti.hardware.servicetracker@1.2-service.rc
-vendor/etc/vintf/manifest/vendor.qti.hardware.servicetracker@1.2-service.xml
vendor/lib64/hw/vendor.qti.hardware.servicetracker@1.2-impl.so

# Snapdragon Computer Vision Engine
vendor/lib/libfastcvdsp_stub.so
-vendor/lib/libfastcvopt.so
vendor/lib/libscveCommon.so
vendor/lib/libscveCommon_stub.so
vendor/lib/libscveObjectSegmentation.so
vendor/lib/libscveObjectSegmentation_stub.so
vendor/lib/libscveObjectTracker.so
vendor/lib/libscveObjectTracker_stub.so
vendor/lib64/libfastcvdsp_stub.so
-vendor/lib64/libfastcvopt.so
vendor/lib64/libscveCommon.so
vendor/lib64/libscveCommon_stub.so
vendor/lib64/libscveObjectSegmentation.so
vendor/lib64/libscveObjectSegmentation_stub.so
vendor/lib64/libscveObjectTracker.so
vendor/lib64/libscveObjectTracker_stub.so
vendor/lib64/vendor.qti.hardware.scve.objecttracker@1.0.so
vendor/lib64/vendor.qti.hardware.scve.panorama@1.0.so

# Soter
vendor/bin/hw/vendor.qti.hardware.soter@1.0-service
vendor/etc/init/vendor.qti.hardware.soter@1.0-service.rc
vendor/lib64/hw/vendor.qti.hardware.soter@1.0-impl.so
vendor/lib64/vendor.qti.hardware.soter@1.0.so

# Time services
-vendor/app/TimeService/TimeService.apk
vendor/bin/time_daemon
vendor/etc/init/init.time_daemon.rc
vendor/lib64/libtime_genoff.so

# Trusted Execution Environment connector
vendor/bin/hw/vendor.qti.hardware.qteeconnector@1.0-service
vendor/etc/init/vendor.qti.hardware.qteeconnector@1.0-service.rc
vendor/lib64/hw/vendor.qti.hardware.qteeconnector@1.0-impl.so
vendor/lib64/libGPQTEEC_vendor.so
vendor/lib64/libGPTEE_vendor.so
vendor/lib64/libQTEEConnector_listener.so
vendor/lib64/libQTEEConnector_vendor.so
vendor/lib64/vendor.qti.hardware.qteeconnector@1.0.so

# Trusted User Interface
vendor/bin/hw/vendor.qti.hardware.tui_comm@1.0-service-qti
vendor/etc/init/vendor.qti.hardware.tui_comm@1.0-service-qti.rc
vendor/lib64/vendor.qti.hardware.tui_comm@1.0.so

# VPP
vendor/lib/vendor.qti.hardware.vpp@1.1.so
vendor/lib/vendor.qti.hardware.vpp@1.2.so
vendor/lib64/vendor.qti.hardware.vpp@1.1.so
vendor/lib64/vendor.qti.hardware.vpp@1.2.so

# Widevine DRM
vendor/bin/hw/android.hardware.drm@1.3-service.widevine
vendor/etc/init/android.hardware.drm@1.3-service.widevine.rc
-vendor/etc/vintf/manifest/manifest_android.hardware.drm@1.3-service.widevine.xml
vendor/lib64/mediadrm/libwvdrmengine.so
vendor/lib64/libminkdescriptor.so
vendor/lib64/liboemcrypto.so
vendor/lib64/libtrustedapploader.so
vendor/lib64/libwvhidl.so

# Wi-Fi
vendor/bin/cnss-daemon
vendor/etc/wifi/aoa_cldb_falcon.bin
vendor/etc/wifi/aoa_cldb_swl14.bin

# WiFi Display
-framework/WfdCommon.jar
system_ext/bin/wfdservice
system_ext/etc/init/wfdservice.rc
system_ext/etc/permissions/wfd-system-ext-privapp-permissions-qti.xml
system_ext/etc/seccomp_policy/wfdservice.policy
system_ext/etc/wfdconfigsink.xml
system_ext/lib/com.qualcomm.qti.wifidisplayhal@1.0.so
system_ext/lib/libmmrtpdecoder.so
system_ext/lib/libmmrtpencoder.so
system_ext/lib/libwfdavenhancements.so
system_ext/lib/libwfdclient.so
system_ext/lib/libwfdcommonutils.so|5543908f1bd14a535a63be847356272861acc79e
system_ext/lib/libwfdconfigutils.so
system_ext/lib/libwfddisplayconfig.so
system_ext/lib/libwfdmminterface.so
system_ext/lib/libwfdmmsink.so
system_ext/lib/libwfdmmsrc_system.so|b994be02397830c19d55ef209ea5c3b561c40a75
system_ext/lib/libwfdrtsp.so
system_ext/lib/libwfdservice.so|fef60293f70968b64a23a80bc33d46e3333c7ab5
system_ext/lib/libwfdsinksm.so
system_ext/lib/libwfduibcinterface.so
system_ext/lib/libwfduibcsink.so
system_ext/lib/libwfduibcsinkinterface.so
system_ext/lib/libwfduibcsrc.so
system_ext/lib/libwfduibcsrcinterface.so
system_ext/lib/vendor.qti.hardware.wifidisplaysession@1.0.so
system/lib64/libinput.so:system_ext/lib64/libwfdinput.so
system_ext/lib64/libmmparser_lite.so
system_ext/lib64/libmmrtpdecoder.so
system_ext/lib64/libmmrtpencoder.so
system_ext/lib64/libwfdclient.so
system_ext/lib64/libwfdcommonutils.so|affb178d4ad733929a0a67059906af19431aae29
system_ext/lib64/libwfdconfigutils.so
system_ext/lib64/libwfddisplayconfig.so
system_ext/lib64/libwfdmminterface.so
system_ext/lib64/libwfdmmsink.so
system_ext/lib64/libwfdnative.so|142eee595c856cfcae05d47d4c7554c220a7a029
system_ext/lib64/libwfdrtsp.so
system_ext/lib64/libwfdsinksm.so
system_ext/lib64/libwfduibcinterface.so
system_ext/lib64/libwfduibcsink.so
system_ext/lib64/libwfduibcsinkinterface.so
system_ext/lib64/libwfduibcsrc.so
system_ext/lib64/libwfduibcsrcinterface.so
-system_ext/priv-app/WfdService/WfdService.apk
vendor/bin/wfdhdcphalservice
vendor/bin/wfdvndservice
vendor/bin/wifidisplayhalservice
vendor/etc/init/android.hardware.drm@1.1-service.wfdhdcp.rc
vendor/etc/init/com.qualcomm.qti.wifidisplayhal@1.0-service.rc
vendor/etc/init/wfdvndservice.rc
vendor/etc/seccomp_policy/wfdhdcphalservice.policy
vendor/etc/seccomp_policy/wfdvndservice.policy
vendor/etc/seccomp_policy/wifidisplayhalservice.policy
vendor/etc/wfdconfig.xml
vendor/lib/libFileMux_proprietary.so
vendor/lib/libmm-hdcpmgr.so
vendor/lib/libmmosal.so
vendor/lib/libmmrtpdecoder_proprietary.so
vendor/lib/libmmrtpencoder_proprietary.so
vendor/lib/libwfdcodecv4l2_proprietary.so
vendor/lib/libwfdcommonutils_proprietary.so
vendor/lib/libwfdconfigutils_proprietary.so
vendor/lib/libwfddisplayconfig_proprietary.so
vendor/lib/libwfdhdcpcp.so
vendor/lib/libwfdhdcpservice_proprietary.so
vendor/lib/libwfdmminterface_proprietary.so
vendor/lib/libwfdmmservice_proprietary.so
vendor/lib/libwfdmmsrc_proprietary.so
vendor/lib/libwfdmodulehdcpsession.so
vendor/lib/libwfdrtsp_proprietary.so
vendor/lib/libwfdsessionmodule.so
vendor/lib/libwfdsourcesession_proprietary.so
vendor/lib/libwfdsourcesm_proprietary.so
vendor/lib/libwfduibcinterface_proprietary.so
vendor/lib/libwfduibcsink_proprietary.so
vendor/lib/libwfduibcsinkinterface_proprietary.so
vendor/lib/libwfduibcsrc_proprietary.so
vendor/lib/libwfduibcsrcinterface_proprietary.so
vendor/lib/libwfdutils_proprietary.so
vendor/lib/vendor.qti.hardware.wifidisplaysession@1.0.so
vendor/lib/vendor.qti.hardware.wifidisplaysessionl@1.0-halimpl.so
